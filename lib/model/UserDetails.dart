class UserDetails {
  Address address;
  Company company;
  String email;
  int id;
  String name;
  String phone;
  String username;
  String website;
  String profileUrl;

  UserDetails.dart(
      {this.address,
      this.company,
      this.email,
      this.id,
      this.name,
      this.phone,
      this.username,
      this.website,
      this.profileUrl = 'https://picsum.photos/200/300'});

  factory UserDetails.fromJson(Map<String, dynamic> json) {
    return UserDetails.dart(
      address:
          json['address'] != null ? Address.fromJson(json['address']) : null,
      company:
          json['company'] != null ? Company.fromJson(json['company']) : null,
      email: json['email'],
      id: json['id'],
      name: json['name'],
      phone: json['phone'],
      username: json['username'],
      website: json['website'],
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['email'] = this.email;
    data['id'] = this.id;
    data['name'] = this.name;
    data['phone'] = this.phone;
    data['username'] = this.username;
    data['website'] = this.website;
    if (this.address != null) {
      data['address'] = this.address.toJson();
    }
    if (this.company != null) {
      data['company'] = this.company.toJson();
    }
    return data;
  }
}

class Company {
  String bs;
  String catchPhrase;
  String name;

  Company({this.bs, this.catchPhrase, this.name});

  factory Company.fromJson(Map<String, dynamic> json) {
    return Company(
      bs: json['bs'],
      catchPhrase: json['catchPhrase'],
      name: json['name'],
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['bs'] = this.bs;
    data['catchPhrase'] = this.catchPhrase;
    data['name'] = this.name;
    return data;
  }
}

class Address {
  String city;
  Geo geo;
  String street;
  String suite;
  String zipcode;

  Address({this.city, this.geo, this.street, this.suite, this.zipcode});

  factory Address.fromJson(Map<String, dynamic> json) {
    return Address(
      city: json['city'],
      geo: json['geo'] != null ? Geo.fromJson(json['geo']) : null,
      street: json['street'],
      suite: json['suite'],
      zipcode: json['zipcode'],
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['city'] = this.city;
    data['street'] = this.street;
    data['suite'] = this.suite;
    data['zipcode'] = this.zipcode;
    if (this.geo != null) {
      data['geo'] = this.geo.toJson();
    }
    return data;
  }
}

class Geo {
  String lat;
  String lng;

  Geo({this.lat, this.lng});

  factory Geo.fromJson(Map<String, dynamic> json) {
    return Geo(
      lat: json['lat'],
      lng: json['lng'],
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['lat'] = this.lat;
    data['lng'] = this.lng;
    return data;
  }
}
/* final int id;
  final String firstName, lastName, profileUrl, email, phone, website;
  final Address address;

  UserDetails(
      {this.id,
      this.firstName,
      this.lastName,
      this.email,
      this.profileUrl = 'https://picsum.photos/200/300',
      this.phone,
      this.website,
      this.address});

  factory UserDetails.fromJson(Map<String, dynamic> json) {
    return new UserDetails(
        id: json['id'],
        firstName: json['name'],
        lastName: json['username'],
        email: json['email'],
        website: json['website'],
        phone: json['phone'],
        address: json['address']);
  }
}

class Address {
  final String street, suite, city, zipcode;

  Address({this.street, this.suite, this.city, this.zipcode});

  factory Address.fromJson(Map<String, dynamic> json) {
    return new Address(
        street: json['street'],
        suite: json['suite'],
        city: json['city'],
        zipcode: json['zipcode']);
  }*/
