import 'dart:ui';

class PushNotification {
  PushNotification({
    this.title,
    this.body,
    this.color
  });

  String title;
  String body;
  final Color color;
}