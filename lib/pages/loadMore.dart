import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:lazy_load_scrollview/lazy_load_scrollview.dart';

import 'DemoItem.dart';

class LoadMorePage extends StatefulWidget {
  LoadMorePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _LoadMorePagePageState createState() => new _LoadMorePagePageState();
}

class _LoadMorePagePageState extends State<LoadMorePage> {
  List<int> verticalData = [];
  List<int> horizontalData = [];

  final int increment = 10;

  bool isLoadingVertical = false;
  bool isLoading = false;
  bool isLoadingHorizontal = false;

  @override
  void initState() {
    _loadMoreVertical();
    _loadMoreHorizontal();
    super.initState();
  }

  Future _loadMoreVertical() async {
    setState(() {
      isLoadingVertical = true;
      isLoading = true;
    });

    // Add in an artificial delay
    await new Future.delayed(const Duration(seconds: 2));

    verticalData.addAll(
        List.generate(increment, (index) => verticalData.length + index));

    setState(() {
      isLoadingVertical = false;
      isLoading = false;
    });
  }

  Future _loadMoreHorizontal() async {
    setState(() {
      isLoadingHorizontal = true;
      isLoading = true;
    });

    // Add in an artificial delay
    await new Future.delayed(const Duration(seconds: 2));

    horizontalData.addAll(
        List.generate(increment, (index) => horizontalData.length + index));

    setState(() {
      isLoadingHorizontal = false;
      isLoading = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: showWidget(),
    );
  }

  showWidget() {
    if (isLoading)
      return loadingView();
    else
      return showWidgetView();
  }

  showWidgetView() {
    return LazyLoadScrollView(
      isLoading: isLoadingVertical,
      onEndOfPage: () => _loadMoreVertical(),
      child: Scrollbar(
        child: ListView(
          children: [
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Text(
                'Nested horizontal ListView',
                textAlign: TextAlign.center,
              ),
            ),
            Container(
                height: 180,
                child: LazyLoadScrollView(
                    isLoading: isLoadingHorizontal,
                    scrollDirection: Axis.horizontal,
                    onEndOfPage: () => _loadMoreHorizontal(),
                    child: Scrollbar(
                        child: ListView.builder(
                            itemCount: horizontalData.length,
                            scrollDirection: Axis.horizontal,
                            itemBuilder: (context, position) {
                              return DemoItem(position);
                            })))),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Text(
                'Vertical ListView',
                textAlign: TextAlign.center,
              ),
            ),
            ListView.builder(
              shrinkWrap: true,
              physics: NeverScrollableScrollPhysics(),
              itemCount: verticalData.length,
              itemBuilder: (context, position) {
                return DemoItem(position);
              },
            ),
          ],
        ),
      ),
    );
  }

  loadingView() {
    return Container(
      width: 50,
      height: 50,
      alignment: Alignment.center,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.all(Radius.circular(18)),
        color: Colors.grey,
      ),
      child: CircularProgressIndicator(),
    );
  }
}
