import 'package:flutter/material.dart';
import 'package:flutter_app/pages/Slidable/SlidableModel.dart';
import 'package:flutter_slidable/flutter_slidable.dart';

class VerticalList extends StatelessWidget {
  VerticalList(this.item);
  final SlidableModal item;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () =>
      Slidable.of(context)?.renderingMode == SlidableRenderingMode.none
          ? Slidable.of(context)?.open()
          : Slidable.of(context)?.close(),
      child: Container(
        color: Colors.white10,
        child: ListTile(
          leading: CircleAvatar(
            backgroundColor: item.color,
            child: Text('${item.index}'),
            foregroundColor: Colors.white,
          ),
          title: Text(item.titles),
          subtitle: Text(item.subtitle),
        ),
      ),
    );
  }
}