import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app/utils/IntelUtility.dart';
import 'package:google_fonts/google_fonts.dart';

class HomeNavigation extends StatefulWidget {
  String title;
  int position = 0;

  HomeNavigation({this.title, this.position});

  @override
  _HomeNavigationState createState() => _HomeNavigationState();
}

class _HomeNavigationState extends State<HomeNavigation> {
  final emailController = TextEditingController();
  final passwordController = TextEditingController();
  bool _passwordVisible = false;

  var databaseRef = FirebaseDatabase.instance.reference();
  final Future<FirebaseApp> _future = Firebase.initializeApp();

  String email;
  String password;

  String tableName = "User_Details";

  @override
  void initState() {
    super.initState();
    _passwordVisible = false;
  }

  bool isAlreadyRegister = false;

  void checkData(String email, String password) {
    databaseRef = FirebaseDatabase.instance.reference().child(tableName);

    databaseRef.once().then((DataSnapshot snapshot)  {
      print('Get Firebase Data : ${snapshot.value}');
      isAlreadyRegister = false;
      if (snapshot.value != null) {
        Map<dynamic, dynamic> values = snapshot.value;
        values.forEach((key, values) {
          var emailValues = values["email"];
          var password = values["password"];

          if (email == emailValues) {
            isAlreadyRegister = true;
          }
          print("========> $emailValues");
          print("========> $password");
        });

         addNewData(email, password);
      } else {
        isAlreadyRegister = false;
         addNewData(email, password);
      }
    });
  }

  void addNewData(String email, String password) {
    if (isAlreadyRegister == false) {
      databaseRef.push().set({'email': email, 'password': password});

      emailController.clear();
      passwordController.clear();
      IntelUtility.showToast(context, "New User Register.");
      IntelUtility.showLoader(false, context);
    } else {
      IntelUtility.showToast(context, "Email already register.");
      IntelUtility.showLoader(false, context);
    }
    // IntelUtility.showToast(context, "Send User details.");
    setState(() {});
  }

  void printFirebase() {
    databaseRef = FirebaseDatabase.instance.reference().child(tableName);
    databaseRef.once().then((DataSnapshot snapshot) {
      print('Get Firebase Data : ${snapshot.value}');

      if (snapshot.value != null) {
        Map<dynamic, dynamic> values = snapshot.value;
        values.forEach((key, values) {
          var email = values["email"];
          var password = values["password"];
          print("========> $email");
          print("========> $password");
        });
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    printFirebase();

    return FutureBuilder(
        future: _future,
        builder: (context, snapshot) {
          if (snapshot.hasError) {
            return Text(snapshot.error.toString());
          } else {
            return SafeArea(
              child: Scaffold(
                resizeToAvoidBottomInset: false,
                body: Stack(
                  children: <Widget>[
                    Container(
                      height: MediaQuery.of(context).size.width * 1,
                      width: double.infinity,
                      color: Colors.blue,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.stretch,
                        children: [
                          SizedBox(
                            height: 50,
                          ),
                          Text(
                            "SignUp in Firebase",
                            textAlign: TextAlign.center,
                            style: GoogleFonts.lato(
                              textStyle: TextStyle(
                                letterSpacing: .5,
                                color: Colors.white,
                                fontSize: 50,
                                fontStyle: FontStyle.normal,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    Card(
                      elevation: 8,
                      color: Colors.white,
                      margin: EdgeInsets.only(
                          top: MediaQuery.of(context).size.width * 0.8,
                          left: 20,
                          right: 20),
                      child: SingleChildScrollView(
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          children: <Widget>[
                            SizedBox(
                              height: 25,
                            ),
                            Container(
                              margin: EdgeInsets.only(
                                  bottom: 20, left: 20, right: 20),
                              child: TextFormField(
                                onChanged: (value) {
                                  email = value;
                                  setState(() {});
                                },
                                controller: emailController,
                                keyboardType: TextInputType.emailAddress,
                                decoration: InputDecoration(
                                    hintText: 'Email',
                                    hintStyle: TextStyle(
                                      fontFamily: "Poppins",
                                      fontSize: 16,
                                      color: Colors.cyan,
                                    )),
                              ),
                            ),
                            SizedBox(
                              height: 15,
                            ),
                            Container(
                              margin: EdgeInsets.only(
                                  bottom: 40, left: 20, right: 20),
                              child: TextFormField(
                                onChanged: (value) {
                                  password = value;
                                  setState(() {});
                                },
                                controller: passwordController,
                                keyboardType: TextInputType.text,
                                decoration: InputDecoration(
                                    hintText: 'Password',
                                    hintStyle: TextStyle(
                                      fontFamily: "Poppins",
                                      fontSize: 16,
                                      color: Colors.cyan,
                                    )),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Container(
                      margin: EdgeInsets.only(
                          top: MediaQuery.of(context).size.width * 1.22,
                          left: 35,
                          right: 35),
                      width: MediaQuery.of(context).size.width,
                      height: 40,
                      decoration: BoxDecoration(
                          color: Colors.red,
                          border: Border.all(
                            color: Colors.red,
                          ),
                          borderRadius: BorderRadius.all(Radius.circular(20))),
                      child: ConstrainedBox(
                        constraints: BoxConstraints.expand(),
                        child: FlatButton(
                          onPressed: () async {
                            print("Clicked Login Button......");
                            email = emailController.text;
                            password = passwordController.text;

                            if (email.trim().isEmpty) {
                              IntelUtility.showToast(
                                  context, "Please enter email");
                            } else if (!IntelUtility.isValidEmail(email)) {
                              IntelUtility.showToast(
                                  context, "Please enter valid email");
                            } else if (password.isEmpty) {
                              IntelUtility.showToast(
                                  context, "Please enter password");
                            } else if (password.length < 6) {
                              IntelUtility.showToast(context,
                                  "Please enter password should be at-least 6");
                            } else {
                              IntelUtility.showLoader(true, context);
                              //  changedButton = true;
                              setState(() {});

                              checkData(email, password);

                              /*new Future.delayed(
                                const Duration(seconds: 3),
                                () {
                                  //  You action here
                                  IntelUtility.showLoader(false, context);
                                  IntelUtility.showAlert(
                                    context,
                                    "Success",
                                    "Login Successfully.",
                                  );
                                },
                              );*/
                            }
                          },
                          padding: EdgeInsets.all(0.0),
                          child: Text(
                            'Submit',
                            style: TextStyle(
                              color: Colors.white,
                              fontFamily: "Poppins",
                              fontSize: 20,
                            ),
                            textAlign: TextAlign.center,
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            );
          }
        });
  }
}
