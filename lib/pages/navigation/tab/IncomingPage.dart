import 'package:flutter/material.dart';

class IncomingPage extends StatefulWidget {
  @override
  _IncomingPageState createState() => _IncomingPageState();
}

class _IncomingPageState extends State<IncomingPage>
    with AutomaticKeepAliveClientMixin<IncomingPage> {
  int count = 10;

  void clear() {
    setState(() {
      count = 0;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Text(
              'Headline',
              style: TextStyle(fontSize: 18),
            ),
            SizedBox(
              height: 50.0,
              child: ListView.builder(
                physics: ClampingScrollPhysics(),
                shrinkWrap: true,
                scrollDirection: Axis.horizontal,
                itemCount: itemsList.length,
                itemBuilder: (BuildContext context, int index) => Card(
                  color: Colors.blueGrey,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(2.0),
                  ),
                  elevation: 4,
                  child: Padding(
                    padding: EdgeInsets.all(15),
                    child: Center(
                      child: Text('${itemsList[index]}'),
                    ),
                  ),
                ),
              ),
            ),
            Text(
              'Demo Headline 2',
              style: TextStyle(fontSize: 18),
            ),
            SizedBox(
              height: MediaQuery.of(context).size.height * 0.75,
              child: ListView.builder(
                physics: ClampingScrollPhysics(),
                shrinkWrap: true,
                scrollDirection: Axis.vertical,
                itemCount: itemsList.length,
                itemBuilder: (BuildContext context, int index) => Card(
                  color: Colors.blue,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(2.0),
                  ),
                  elevation: 4,
                  child: Padding(
                    padding: EdgeInsets.all(15),
                    child: Center(
                      child: Text(
                        '${itemsList[index]}',
                      ),
                    ),
                  ),
                ),
              ),
            ),
            /* Container(
              child: ListView.builder(
                physics: ClampingScrollPhysics(),
                shrinkWrap: true,
                scrollDirection: Axis.vertical,
                itemCount: itemsList.length,
                itemBuilder: (BuildContext context, int index) => Card(
                  child: Center(child: Text('${itemsList[index]}')),
                ),
              ),
            ),*/
            /* Card(
              child: ListTile(
                  title: Text('Motivation $int'),
                  subtitle: Text('this is a description of the motivation')),
            ),
            Card(
              child: ListTile(
                  title: Text('Motivation $int'),
                  subtitle: Text('this is a description of the motivation')),
            ),
            Card(
              child: ListTile(
                  title: Text('Motivation $int'),
                  subtitle: Text('this is a description of the motivation')),
            ),
            Card(
              child: ListTile(
                  title: Text('Motivation $int'),
                  subtitle: Text('this is a description of the motivation')),
            ),
            Card(
              child: ListTile(
                  title: Text('Motivation $int'),
                  subtitle: Text('this is a description of the motivation')),
            ),
            Card(
              child: ListTile(
                  title: Text('Motivation $int'),
                  subtitle: Text('this is a description of the motivation')),
            ), Card(
              child: ListTile(
                  title: Text('Motivation $int'),
                  subtitle: Text('this is a description of the motivation')),
            ), Card(
              child: ListTile(
                  title: Text('Motivation $int'),
                  subtitle: Text('this is a description of the motivation')),
            ), Card(
              child: ListTile(
                  title: Text('Motivation $int'),
                  subtitle: Text('this is a description of the motivation')),
            ), Card(
              child: ListTile(
                  title: Text('Motivation $int'),
                  subtitle: Text('this is a description of the motivation')),
            ),*/
          ],
        ),
      ),
    );
    /*return SingleChildScrollView(
      child:  Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          ListView(
            scrollDirection: Axis.vertical,
            shrinkWrap: true,
            children: <Widget>[
              //horizontal list required Height provide
              horizontalList1(),
              verticleList(),
            ],
          ),
        ],
      ),
    );*/
  }

  final itemsList = List<String>.generate(10000, (i) => "Row $i");

  Widget horizontalList1() {
    return new Container(
      margin: EdgeInsets.symmetric(vertical: 20.0),
      height: 50.0,
      child: new ListView.builder(
        shrinkWrap: true,
        physics: ClampingScrollPhysics(),
        scrollDirection: Axis.horizontal,
        itemCount: itemsList.length,
        itemBuilder: (BuildContext context, int index) => Card(
          elevation: 8.0,
          child: Padding(
            padding: const EdgeInsets.all(4.0),
            child: Center(
              child: Text(
                '${itemsList[index]}',
                style: TextStyle(fontSize: 15),
              ),
            ),
          ),
        ),
      ),
    );
  }

  Widget verticleList() {
    return Flexible(
      child: itemsList.length != 0 || itemsList.length != null
          ? ListView.builder(
              shrinkWrap: true,
              scrollDirection: Axis.vertical,
              itemCount: itemsList.length,
              itemBuilder: (BuildContext context, int index) => Card(
                elevation: 8.0,
                child: Padding(
                  padding: const EdgeInsets.all(4.0),
                  child: Center(
                    child: Text(
                      '${itemsList[index]}',
                      style: TextStyle(fontSize: 15),
                    ),
                  ),
                ),
              ),
              /*  itemBuilder: (context, index) {
                return Card(
                  child: ListTile(
                    title: Text(
                      '${itemsList[index]}',
                      style: TextStyle(fontSize: 15),
                    ),
                  ),
                  margin: const EdgeInsets.all(0.0),
                );
              },*/
              /* children: <Widget>[
          Container(
            width: 160.0,
            color: Colors.blue,
          ),
          Container(
            width: 160.0,
            color: Colors.green,
          ),
          Container(
            width: 160.0,
            color: Colors.cyan,
          ),
          Container(
            width: 160.0,
            color: Colors.black,
          ),
        ],*/
            )
          : Container(),
    );
  }

  @override
  bool get wantKeepAlive => true;
}
