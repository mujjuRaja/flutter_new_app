import 'package:flutter/material.dart';
import 'package:flutter_app/pages/navigation/tab/IncomingPage.dart';
import 'package:flutter_app/pages/navigation/tab/FcmRealTimePage.dart';
import 'package:flutter_app/pages/navigation/tab/OutgoingPage.dart';

class TabLayoutScreen extends StatelessWidget {
  const TabLayoutScreen();

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 3,
      child: Scaffold(
        appBar: AppBar(
          flexibleSpace: Column(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              TabBar(
                tabs: [
                  Tab(
                    text: 'Incoming',
                  ),
                  Tab(
                    text: 'Outgoing',
                  ),
                  Tab(
                    text: 'Missed',
                  ),
                ],
              ),
            ],
          ),
        ),
        body: TabBarView(
          children: [
            IncomingPage(),
            OutgoingPage(),
            FcmRealTimePage(),
          ],
        ),
      ),
    );
  }
}
