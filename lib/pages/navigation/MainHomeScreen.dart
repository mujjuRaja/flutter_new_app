import 'package:flutter/material.dart';
import 'package:flutter_app/pages/GoogleMap.dart';
import 'package:flutter_app/pages/navigation/tab/TabLayoutScreen.dart';

import '../homescreen.dart';
import 'HomeNavigation.dart';

class MainHomeScreen extends StatefulWidget {
  @override
  _MainHomeScreenState createState() => _MainHomeScreenState();
}

class _MainHomeScreenState extends State<MainHomeScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: _widgetOptions.elementAt(_selectedIndex),
      bottomNavigationBar: bottomNavigationData(),
    );
  }

  int _selectedIndex = 0;

  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  List<Widget> _widgetOptions = <Widget>[
    HomeNavigation(),
    homeScreen(),
    TabLayoutScreen(),
    GoogleMapPage(),
  ];

  Widget bottomNavigationData() {
    return Container(
      decoration: BoxDecoration(
        boxShadow: <BoxShadow>[
          BoxShadow(
            color: Colors.black,
            blurRadius: 10,
          ),
        ],
      ),
      child: BottomNavigationBar(
        elevation: 10,
        items: [
          BottomNavigationBarItem(
            icon: Image.asset(
              "assets/icons/ic_home.png",
              height: 25,
              width: 25,
            ),
            label: "Home",
            activeIcon: Image.asset(
              "assets/icons/ic_home.png",
              height: 25,
              width: 25,
              color: Colors.amber[800],
            ),
          ),
          BottomNavigationBarItem(
            icon: Image.asset(
              "assets/icons/ic_user.png",
              height: 25,
              width: 25,
            ),
            label: "Profile",
            activeIcon: Image.asset(
              "assets/icons/ic_user.png",
              height: 25,
              width: 25,
              color: Colors.amber[800],
            ),
          ),
          BottomNavigationBarItem(
            icon: Image.asset(
              "assets/icons/ic_tab.png",
              height: 25,
              width: 25,
            ),
            label: "Tab",
            activeIcon: Image.asset(
              "assets/icons/ic_tab.png",
              height: 25,
              width: 25,
              color: Colors.amber[800],
            ),
          ),
          BottomNavigationBarItem(
            icon: Image.asset(
              "assets/icons/ic_google_map.png",
              height: 25,
              width: 25,
            ),
            label: "Map",
            activeIcon: Image.asset(
              "assets/icons/ic_google_map.png",
              height: 25,
              width: 25,
              color: Colors.amber[800],
            ),
          ),
        ],
        currentIndex: _selectedIndex,
        selectedItemColor: Colors.amber[800],
        onTap: _onItemTapped,
      ),
    );
  }
}
