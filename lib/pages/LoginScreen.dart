import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app/listdata/ListBuildData.dart';
import 'package:flutter_app/utils/ColorExtenstion.dart';
import 'package:flutter_app/utils/Constant.dart';
import 'package:flutter_app/utils/IntelUtility.dart';

class LoginScreen extends StatefulWidget {
  @override
  _LoginScreenState createState() => new _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  bool _passwordVisible = false;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _passwordVisible = false;
  }

  final emailController = TextEditingController();
  final passwordController = TextEditingController();
  final _newpasswordController = TextEditingController();
  String _img = 'assets/app_logo.jpg';
  String name = "";

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: AppBar(
        backgroundColor: Colors.green,
        title: Text("Login Screen"),
        elevation: 4,
      ),
      body: SingleChildScrollView(
        child: Container(
          color: Colors.black12,
          child: Container(
              height: MediaQuery.of(context).size.height * 0.8,
              width: MediaQuery.of(context).size.width,
              //Todo:Regarding top space managed as per view
              margin: EdgeInsets.only(
                top: 10,
              ),
              child: Container(
                //Todo: Set top and bottom side background view with round shape
                padding: EdgeInsets.all(25),
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: new BorderRadius.only(
                    topLeft: const Radius.circular(20.0),
                    topRight: const Radius.circular(20.0),
                    bottomLeft: const Radius.circular(20.0),
                    bottomRight: const Radius.circular(20.0),
                  ),
                ),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    IconButton(
                      icon: Image.asset(_img),
                      iconSize: 80,
                      alignment: Alignment.center,
                      onPressed: (){},
                    ),
                    SizedBox(height: 20),
                    Text(
                      "LOGIN $name",
                      style: AppTheme.boldTextStyle()
                          .copyWith(fontSize: 30, color: HexColor("#000FFF")),
                    ),
                    SizedBox(
                      height: MediaQuery.of(context).size.height * 0.05,
                    ),
                    Text(
                      "Email",
                      style: AppTheme.normalTextStyle().copyWith(fontSize: 16),
                    ),
                    SizedBox(
                      height: MediaQuery.of(context).size.height * 0.02,
                    ),
                    TextField(
                      controller: emailController,
                      keyboardType: TextInputType.emailAddress,
                      onChanged: (value) {
                        name = value;
                        setState(() {});
                      },
                      decoration: InputDecoration(
                          isDense: true,
                          contentPadding: EdgeInsets.all(15),
                          labelText: "",
                          hintText: "Please enter email-id",
                          filled: true,
                          fillColor: HexColor("#FBFBFB"),
                          enabledBorder: OutlineInputBorder(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(10)),
                              borderSide:
                                  BorderSide(color: HexColor("#ECECEC"))),
                          focusedBorder: OutlineInputBorder(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(10)),
                              borderSide: BorderSide(
                                  color: AppTheme.mainThemeColor()))),
                    ),
                    SizedBox(
                      height: MediaQuery.of(context).size.height * 0.03,
                    ),
                    Text(
                      "Password",
                      style: AppTheme.normalTextStyle().copyWith(fontSize: 16),
                    ),
                    SizedBox(
                      height: MediaQuery.of(context).size.height * 0.02,
                    ),
                    Container(
                      height: 70,
                      child: TextField(
                        obscureText: true,
                        controller: passwordController,
                        decoration: InputDecoration(
                          isDense: true,
                          contentPadding: EdgeInsets.all(15),
                          labelText: "",
                          filled: true,
                          fillColor: HexColor("#FBFBFB"),
                          enabledBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.all(
                              Radius.circular(10),
                            ),
                            borderSide: BorderSide(
                              color: HexColor("#ECECEC"),
                            ),
                          ),
                          focusedBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.all(
                              Radius.circular(10),
                            ),
                            borderSide: BorderSide(
                              color: AppTheme.mainThemeColor(),
                            ),
                          ),
                        ),
                      ),
                    ),
                    SizedBox(
                      height: MediaQuery.of(context).size.height * 0.02,
                    ),
                    TextFormField(
                      keyboardType: TextInputType.text,
                      controller: _newpasswordController,
                      obscureText: !_passwordVisible,
                      //This will obscure text dynamically
                      decoration: InputDecoration(
                        labelText: 'New Password',
                        hintText: 'Enter your new password',
                        // Here is key idea
                        suffixIcon: IconButton(
                          icon: Icon(
                            // Based on passwordVisible state choose the icon
                            _passwordVisible
                                ? Icons.visibility
                                : Icons.visibility_off,
                            color: Theme.of(context).primaryColorDark,
                          ),
                          onPressed: () {
                            // Update the state i.e. toogle the state of passwordVisible variable
                            setState(() {
                              _passwordVisible = !_passwordVisible;
                            });
                          },
                        ),
                      ),
                    ),
                    GestureDetector(
                      onTap: () {},
                      child: Container(
                        alignment: Alignment.centerRight,
                        child: Text(
                          "Forgot Password",
                          style: AppTheme.boldTextStyle().copyWith(
                              fontSize: 16, color: HexColor("#49873F")),
                        ),
                      ),
                    ),
                    SizedBox(
                      height: MediaQuery.of(context).size.height * 0.05,
                    ),
                    logInButtonView(context)
                  ],
                ),
              )),
        ),
      ),
    );
  }

  var email = "";
  var password = "";
  var new_password = "";
  bool changedButton = false;

  logInButtonView(BuildContext context) {
    return AnimatedContainer(
      duration: Duration(seconds: 1),
      //width: MediaQuery.of(context).size.width,
      width: changedButton ? 50 : 150,
      height: 50,
      decoration: BoxDecoration(
        color: HexColor("#94C6F7"),
      shape: changedButton
        ? BoxShape.circle
        : BoxShape.rectangle,
        //borderRadius: BorderRadius.all(Radius.circular(10)),
      ),

      child: FlatButton(
        textColor: Colors.white,
        onPressed: () async {
          email = emailController.text;
          password = passwordController.text;
          new_password = _newpasswordController.text;

          // IntelUtility.loading(true, context);
          if (email.trim().isEmpty) {
            IntelUtility.showToast(context, "Please enter email");
          }else if (!IntelUtility.isValidEmail(email)) {
            IntelUtility.showToast(context, "Please enter valid email");
          } else if (password.isEmpty) {
            IntelUtility.showToast(context, "Please enter password");
          } else if (password.length<6) {
            IntelUtility.showToast(context, "Please enter password should be at-least 6");
          } else if (new_password.isEmpty) {
            IntelUtility.showToast(context, "Please enter new password");
          } else {
            changedButton = true;
            setState(() {});

            IntelUtility.showToast(context, "Login Successfully.");
            // IntelUtility.loading(false, context);
            await Future.delayed(Duration(seconds: 1),);
            Navigator.push(context,MaterialPageRoute(builder: (context) => ListBuildData()));
          }


        },
        child: changedButton ? Icon(Icons.done, color: Colors.white,): Text(
          "Submit",
          style: AppTheme.normalTextStyle()
              .copyWith(fontSize: 16, color: Colors.white),
        ),
      ),
    );
  }
}
