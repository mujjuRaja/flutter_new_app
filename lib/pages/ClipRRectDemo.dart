import 'package:flutter/material.dart';


class ClipRRectDemo extends StatefulWidget {
  @override
  _ClipRRectDemoState createState() => _ClipRRectDemoState();
}

class _ClipRRectDemoState extends State<ClipRRectDemo> {
  double _height;
  double _width;
  String _img = 'assets/app_logo.png';

  @override
  Widget build(BuildContext context) {
    _height = MediaQuery.of(context).size.height;
    _width = MediaQuery.of(context).size.width;
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.blue[300],
        title: Text('ClipRRect'),
        centerTitle: true,
      ),
      body: Container(
        padding: EdgeInsets.all(20),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            Center(
                child: ClipRRect(
                  borderRadius: BorderRadius.all(Radius.circular(15)),
                  child: Image.asset(_img),
                )),
            MaterialButton(
              onPressed: () {
                //Navigator.of(context).pushNamed(RouteName.CliPathScreen);
                Navigator.pop(context);
              },
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.all(Radius.circular(15.0))),
              height: _height / 18,
              minWidth: _width,
              elevation: 0,
              color: Colors.blue[300],
              padding: EdgeInsets.all(16.0),
              child: Text('Next',
                  style: TextStyle(
                    fontSize: 15,
                    fontWeight: FontWeight.w600,
                    fontFamily: 'Roboto Medium',
                    color: Colors.white,
                  )),
            ),
          ],
        ),
      ),
    );
  }
}