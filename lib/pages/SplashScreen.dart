import 'package:flutter/material.dart';
import 'package:flutter_app/pages/GoogleMap.dart';
import 'package:flutter_app/pages/navigation/MainHomeScreen.dart';
import 'package:flutter_app/utils/Constant.dart';

import 'NotificationShowPage.dart';
import 'homescreen.dart';

class SplashScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return SplashScreenState();
  }
}

class SplashScreenState extends State<SplashScreen>
    with SingleTickerProviderStateMixin {
  @override
  void initState() {
    super.initState();
    redirectToScreen();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Stack(
      fit: StackFit.expand,
      children: <Widget>[
        Container(
          width: double.infinity,
          height: double.infinity,
          child: Image.asset(IconNames.splashbg, fit: BoxFit.fill),
        ),
        Container(
          margin:
              EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.33),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Image.asset(IconNames.applogo, height: 185, width: 185),
              SizedBox(height: 20),
              Text("Flutter app",
                  style: TextStyle(
                    fontSize: 15,
                    color: Colors.black,
                  ),
                  textAlign: TextAlign.center)
            ],
          ),
        )
      ],
    ));
  }

  redirectToScreen() {
    Future.delayed(Duration(seconds: 3)).then((v) {
      Navigator.of(context).pushReplacement(MaterialPageRoute(
          builder: (BuildContext context) => MainHomeScreen()));
    });
  }
}
