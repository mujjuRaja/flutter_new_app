import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_app/pages/BspAddressmapscreen.dart';
import 'package:flutter_app/pages/ListingSearchPage.dart';
import 'package:flutter_app/pages/MapDrawRoute.dart';
import 'package:flutter_app/pages/PaginationScreen.dart';
import 'package:flutter_app/pages/Slidable/SlidableScreen.dart';
import 'package:flutter_app/pages/VideoPlayerScreen.dart';
import 'package:flutter_app/pages/phoneFcmAuth/PhoneAuthScreen.dart';
import 'package:flutter_app/pages/signinauth/SignInScreen.dart';
import 'package:flutter_app/pages/video/ChewieVideoScreen.dart';
import 'package:geocoder/geocoder.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:location/location.dart';

class AddressForm extends StatefulWidget {
  final Set<Marker> markers;
  final LatLng initialPosition;

  AddressForm({this.markers, this.initialPosition});

  @override
  _AddressFormState createState() => _AddressFormState();
}

class _AddressFormState extends State<AddressForm> {
  LocationData currentLocation;
  String currentAddressShow = "";
  String mapPinAddressShow = "";

  @override
  void initState() {
    super.initState();
    //Todo: using latitude and longitude thru get Address name
    getUserCurrentLocation();
    getMarkerLocation(widget.initialPosition);
    //setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.deepPurple,
        actions: [
          IconButton(
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => PaginationScreen()),
              );
            },
            icon: Icon(
              Icons.restore_page,
              color: Colors.white,
            ),
          ),
          IconButton(
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => SlidableScreen()),
              );
            },
            icon: Icon(
              Icons.account_balance,
              color: Colors.white,
            ),
          ),
          IconButton(
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => VideoPlayerScreen()),
              );
            },
            icon: Icon(
              Icons.video_collection,
              color: Colors.white,
            ),
          ),
          IconButton(
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => ChewieVideoScreen()),
              );
            },
            icon: Icon(
              Icons.video_collection_outlined,
              color: Colors.white,
            ),
          ),
          IconButton(
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => ListingSearchPage()),
              );
            },
            icon: Icon(
              Icons.assignment_ind,
              color: Colors.white,
            ),
          ),
        ],
        bottom: PreferredSize(
          child: Padding(
            padding: const EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 0.0),
            child: Container(
              color: Colors.deepPurple,
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.start,
               // mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  IconButton(
                    onPressed: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => BspAddressmapscreen()),
                      );
                    },
                    icon: Icon(
                      Icons.map,
                      color: Colors.white,
                    ),
                  ),
                  IconButton(
                    onPressed: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(builder: (context) => MapDrawRoute()),
                      );
                    },
                    icon: Icon(
                      Icons.room_outlined,
                      color: Colors.white,
                    ),
                  ),
                  IconButton(
                    onPressed: _openBottomView,
                    icon: Icon(
                      Icons.border_bottom,
                      color: Colors.white,
                    ),
                  ),
                  IconButton(
                    onPressed: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => ListingSearchPage()),
                      );
                    },
                    icon: Icon(
                      Icons.list_alt_outlined,
                      color: Colors.white,
                    ),
                  ),
                  IconButton(
                    onPressed: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => SignInScreen()),
                      );
                    },
                    icon: Icon(
                      Icons.autorenew_outlined,
                      color: Colors.white,
                    ),
                  ),
                  IconButton(
                    onPressed: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => PhoneAuthScreen()),
                      );
                    },
                    icon: Icon(
                      Icons.phone,
                      color: Colors.white,
                    ),
                  ),
                ],
              ),
            ),
          ),
          preferredSize: Size(0.0, 80.0),
        ),
      ),
      body: Container(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            SizedBox(
              height: 25,
            ),
            Text(
              widget.initialPosition.toString(),
              textAlign: TextAlign.center,
              style: TextStyle(
                fontStyle: FontStyle.italic,
                color: Colors.red,
                fontSize: 20,
              ),
            ),
            SizedBox(
              height: 15,
            ),
            Container(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Text(
                    "Current Address: ",
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      fontStyle: FontStyle.italic,
                      color: Colors.red,
                      fontSize: 20,
                    ),
                  ),
                  Text(
                    currentAddressShow,
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      fontStyle: FontStyle.italic,
                      color: Colors.red,
                      fontSize: 20,
                    ),
                  ),
                ],
              ),
            ),
            SizedBox(
              height: 15,
            ),
            Container(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Text(
                    "Map Pin Address: ",
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      fontStyle: FontStyle.italic,
                      color: Colors.red,
                      fontSize: 20,
                    ),
                  ),
                  Text(
                    mapPinAddressShow,
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      fontStyle: FontStyle.italic,
                      color: Colors.red,
                      fontSize: 20,
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  _openBottomView() {
    return showModalBottomSheet(
      context: context,
      shape: RoundedRectangleBorder(
        //Todo: Bottom View full border set, all size: top,bottom,right,left
        //borderRadius: BorderRadius.circular(10.0),

        //Todo: Bottom View set border as per require:: Top/Bottom
        borderRadius: BorderRadius.vertical(
          top: Radius.circular(20),
        ),
      ),
      builder: (context) {
        return Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            ListTile(
              leading: new Icon(Icons.photo),
              title: new Text('Photo'),
              onTap: () {
                Navigator.pop(context);
              },
            ),
            ListTile(
              leading: new Icon(Icons.music_note),
              title: new Text('Music'),
              onTap: () {
                Navigator.pop(context);
              },
            ),
            ListTile(
              leading: new Icon(Icons.videocam),
              title: new Text('Video'),
              onTap: () {
                Navigator.pop(context);
              },
            ),
            ListTile(
              leading: new Icon(Icons.share),
              title: new Text('Share'),
              onTap: () {
                Navigator.pop(context);
              },
            ),
          ],
        );
      },
    );
  }

  getUserCurrentLocation() async {
    //call this async method from whereever you need

    LocationData myLocation;
    String error;
    Location location = new Location();
    try {
      myLocation = await location.getLocation();
    } on PlatformException catch (e) {
      if (e.code == 'PERMISSION_DENIED') {
        error = 'please grant permission';
        print(error);
      }
      if (e.code == 'PERMISSION_DENIED_NEVER_ASK') {
        error = 'permission denied- please enable it from app settings';
        print(error);
      }
      myLocation = null;
    }

    currentLocation = myLocation;
    final coordinates =
        new Coordinates(myLocation.latitude, myLocation.longitude);

    var addresses =
        await Geocoder.local.findAddressesFromCoordinates(coordinates);

    var first = addresses.first;

    print(
        ' ${first.locality}, ${first.adminArea},${first.subLocality}, ${first.subAdminArea},${first.addressLine}, ${first.featureName},${first.thoroughfare}, ${first.subThoroughfare}');

    setState(() {
      currentAddressShow = first.addressLine;
    });
    return first;
  }

  getMarkerLocation(LatLng initialPosition) async {
    //call this async method from whereever you need

    // LocationData myLocation;
    String error;
    Location location = new Location();

    final coordinates =
        new Coordinates(initialPosition.latitude, initialPosition.longitude);

    var addresses =
        await Geocoder.local.findAddressesFromCoordinates(coordinates);

    var first = addresses.first;

    print(
        ' ${first.locality}, ${first.adminArea},${first.subLocality}, ${first.subAdminArea},${first.addressLine}, ${first.featureName},${first.thoroughfare}, ${first.subThoroughfare}');

    setState(() {
      mapPinAddressShow = first.addressLine;
    });
    return first;
  }
}
