import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app/listdata/ProductListPage.dart';
import 'package:flutter_app/pages/ClipRRectDemo.dart';
import 'package:flutter_app/utils/Constant.dart';
import 'package:flutter_app/utils/IntelUtility.dart';
import 'package:google_fonts/google_fonts.dart';

import 'AddExpandListData.dart';
import 'ClipPathDemo.dart';
import 'LoginScreen.dart';
import 'PaginationDataPage.dart';
import 'SliverViewScreen.dart';
import 'loadMore.dart';

class homeScreen extends StatelessWidget {
  List<String> images = [
    "https://static.javatpoint.com/tutorial/flutter/images/flutter-logo.png",
    "https://picsum.photos/200/300",
    "https://picsum.photos/seed/picsum/200/300",
    "https://picsum.photos/200/300",
    "https://picsum.photos/200/300?grayscale",
    "https://picsum.photos/200/300",
    "https://picsum.photos/200/300",
    "https://picsum.photos/200/300",
    "https://picsum.photos/200/300",
    "https://picsum.photos/200/300",
    "https://picsum.photos/200/300",
    "https://picsum.photos/200/300"
  ];

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        appBar: PreferredSize(
          preferredSize: Size.fromHeight(40.0),
          child: AppBar(
            iconTheme: IconThemeData(color: Colors.green),
            title: Text("Flutter Demo"),
            backgroundColor: Colors.white,
            actions: [
              IconButton(
                onPressed: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => SliverViewScreen()),
                  );
                },
                icon: Icon(
                  Icons.settings,
                  color: Colors.black,
                ),
              ),
              IconButton(
                onPressed: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) =>
                            LoadMorePage(title: 'Lazy Load Demo')),
                  );
                },
                icon: Icon(
                  Icons.cloud_download,
                  color: Colors.black,
                ),
              ),
              IconButton(
                onPressed: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => LoginScreen()),
                  );
                },
                icon: Icon(
                  Icons.login,
                  color: Colors.black,
                ),
              ),
              IconButton(
                onPressed: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) =>
                            AddExpandListData(title: 'Expandable List')),
                  );
                },
                icon: Icon(
                  Icons.keyboard_arrow_down,
                  color: Colors.black,
                ),
              ),
              IconButton(
                onPressed: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => ClipPathDemo(),
                    ),
                  );
                },
                icon: Icon(
                  Icons.height,
                  color: Colors.black,
                ),
              ),
              IconButton(
                onPressed: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => ClipRRectDemo(),
                    ),
                  );
                },
                icon: Icon(
                  Icons.error,
                  color: Colors.black,
                ),
              ),
            ],
          ),
        ),
        body: SingleChildScrollView(
          //physics: ScrollPhysics(),
          child: gridViewData(),
        ),
        //Todo: Drawer Data....
        drawer: Drawer(
          child: ListView(
            padding: EdgeInsets.zero,
            children: [
              DrawerHeader(
                padding: EdgeInsets.zero,
                margin: EdgeInsets.zero,
                child: UserAccountsDrawerHeader(
                  decoration: BoxDecoration(
                    color: Colors.green,
                  ),
                  accountName: Text('abc'),
                  accountEmail: Text("abc@gmail.com"),
                  currentAccountPicture: CircleAvatar(
                    backgroundImage: AssetImage(IconNames.applogo),
                  ),
                ),
              ),
              ListTile(
                leading: Icon(
                  CupertinoIcons.home,
                  color: Colors.black,
                ),
                title: Text(
                  "Home",
                  textScaleFactor: 1.2,
                  style: TextStyle(
                    color: Colors.black,
                  ),
                ),
                onTap: () {
                  IntelUtility.showToast(context, "Home Clicked.");
                  print("Home Clicked.");
                },
              ),
              ListTile(
                leading: Icon(
                  CupertinoIcons.profile_circled,
                  color: Colors.black,
                ),
                title: Text(
                  "Profile",
                  textScaleFactor: 1.2,
                  style: TextStyle(
                    color: Colors.black,
                  ),
                ),
                onTap: () {
                  IntelUtility.showToast(context, "Profile Clicked.");
                  print("Profile Clicked.");
                },
              ),
              ListTile(
                leading: Icon(
                  CupertinoIcons.mail,
                  color: Colors.black,
                ),
                title: Text(
                  "List",
                  textScaleFactor: 1.2,
                  style: TextStyle(
                    color: Colors.black,
                  ),
                ),
                onTap: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => ProductListPage()),
                  );
                  print("Email Clicked.");
                },
              ),
              ListTile(
                leading: Icon(
                  Icons.pages,
                  color: Colors.black,
                ),
                title: Text(
                  "Pagination",
                  textScaleFactor: 1.2,
                  style: TextStyle(
                    color: Colors.black,
                  ),
                ),
                onTap: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => PaginationDataPage(),
                    ),
                  );
                  IntelUtility.showToast(context, "Pagination Clicked.");
                  print("Pagination Clicked.");
                },
              ),
            ],
          ),
        ),
      ),
    );
  }

  gridViewData() {
    return GridView.builder(
      //If gridview list not scroll so used this below line code.
      physics: NeverScrollableScrollPhysics(),
      //If gridview not load so used this below line code.
      shrinkWrap: true,
      itemCount: images.length,
      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: 2, crossAxisSpacing: 4.0, mainAxisSpacing: 4.0),

      itemBuilder: (BuildContext context, int index) {
        return Card(
          clipBehavior: Clip.antiAlias,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(15),
          ),
          //  child: Image.network(images[index]),
          child: GridTile(
            header: Container(
              child: Text(
                "Title",
                textAlign: TextAlign.center,
                style: GoogleFonts.poppins(
                  textStyle: TextStyle(
                    fontSize: 14,
                    color: Colors.white,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
              padding: const EdgeInsets.all(15),
              decoration: BoxDecoration(color: Colors.deepPurple),
            ),
            child: Image.network(
              images[index],
              fit: BoxFit.fill,
              loadingBuilder: (BuildContext context, Widget child,
                  ImageChunkEvent loadingProgress) {
                if (loadingProgress == null) {
                  return child;
                }

                return Center(
                  child: CircularProgressIndicator(
                    value: loadingProgress.expectedTotalBytes != null
                        ? loadingProgress.cumulativeBytesLoaded /
                            loadingProgress.expectedTotalBytes
                        : null,
                  ),
                );
              },
            ),
            footer: Container(
              child: Text(
                "\$500",
                textAlign: TextAlign.center,
                style: GoogleFonts.poppins(
                  textStyle: TextStyle(
                    letterSpacing: .5,
                    fontSize: 16,
                    color: Colors.white,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
              padding: const EdgeInsets.all(15),
              decoration: BoxDecoration(
                color: Colors.black38,
              ),
            ),
          ),

          elevation: 8,
        );
      },
    );
  }
}
