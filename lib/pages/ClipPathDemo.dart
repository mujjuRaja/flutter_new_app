import 'package:flutter/material.dart';
import 'package:flutter_app/utils/ClipPathClass.dart';

import 'PageViewDemo.dart';

class ClipPathDemo extends StatefulWidget {
  @override
  _ClipPathDemoState createState() => _ClipPathDemoState();
}

class _ClipPathDemoState extends State<ClipPathDemo> {
  double _height;
  double _width;

  String _imageUrl = 'assets/app_logo.png';

  @override
  Widget build(BuildContext context) {
    _height = MediaQuery.of(context).size.height;
    _width = MediaQuery.of(context).size.width;
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.blue[300],
        title: Text('ClipPath'),
        centerTitle: true,
        actions: [
          IconButton(
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => PageViewDemo()),
              );
            },
            icon: Icon(
              Icons.login,
              color: Colors.white,
            ),
          ),
        ],
      ),
      body: Container(
        margin: EdgeInsets.only(left: 15, right: 15),
        alignment: Alignment.center,
        child: ClipPath(
          clipper: ClipPathClass(),
          child: SizedBox(
            width: 320,
            height: 240,
            child: Image.asset(
              _imageUrl,
              fit: BoxFit.cover,
            ),
          ),
        ),
      ),
    );
  }
}