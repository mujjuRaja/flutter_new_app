import 'package:connectivity/connectivity.dart';
import 'package:flutter/material.dart';

import 'ColorExtenstion.dart';

class Constant {
  static BuildContext context;
  static bool isAlreadyLaunched=false;
  static Color background = HexColor("#D2D9D3");
  static Color backgroundColor = HexColor("#30564D");

  static Future<bool> checkInternet() async {
    var connectivityResult = await (Connectivity().checkConnectivity());
    if (connectivityResult == ConnectivityResult.mobile) {
      return true;
    } else if (connectivityResult == ConnectivityResult.wifi) {
      return true;
    }
    return false;
  }
}

class AppTheme {
  static Color mainThemeColor() {
    return HexColor("#091D05");
  }

  static Color lightGreenColor() {
    return HexColor("#49873F");
  }

  static Color hintTextColor() {
    return HexColor("#BBBCBC");
  }

  static Color textFieldTextColor() {
    return HexColor("#CFCFCF");
  }

  static Color borderColor() {
    return HexColor("#DEDEDE");
  }

  static Color textColor() {
    return HexColor("#010200");
  }

  static Color backgroundColour() {
    return HexColor("#E5E5E5");
  }

  static Color greenColour() {
    return HexColor("#49873F");
  }

  static Color activeFillColour() {
    return HexColor("#D2D2D2");
  }

  static Color profileTextColour() {
    return HexColor("#7F8181");
  }

  static Color whiteColor() {
    return HexColor("#ffffff");
  }

  static Color blackColor() {
    return HexColor("#000000");
  }

  static TextStyle normalTextStyle() {
    return TextStyle(
        fontSize: 12, color: textColor(),);
  }

  static TextStyle boldTextStyle() {
    return TextStyle(
        fontSize: 15, color: textColor(), );
  }

  static TextStyle textFieldTextStyle() {
    return TextStyle(
      fontSize: 14,
      fontWeight: FontWeight.normal,
      color: Colors.black,
    );
  }

  static TextStyle textStyleWith(
      double fontSize, Color color, String fontName) {
    return TextStyle(fontSize: fontSize, color: color, fontFamily: fontName);
  }

  static TextStyle textFieldHintTextStyle() {
    return TextStyle(
        fontSize: 14,
        fontWeight: FontWeight.normal,
        color: AppTheme.textFieldTextColor());
  }
}

class ValidationResult {
  var message = "";
  var isValid = false;

  ValidationResult(this.isValid, this.message);
}

class IconNames {
  static const loading = "assets/loading.json";
  static const applogo = "assets/app_logo.jpg";
  static const splashbg="assets/icons/splash.jpg";
}

class GoogleMapKey{
  static const mapKeyMM1 = "AIzaSyAs8IjIBBwTkiAPZDkBUkABM4yWtZOulzc";
  static const mapKeyMM = "AIzaSyCcyTdIli2NRB3VHcKm5ws8eriCCgD9idw";
  static const mapKeyHar= "AIzaSyBFOpd-OYJgRV3dgiRZ_tid9By1s0M2vP8";
}