import 'package:flutter/material.dart';

class HexColor extends Color {
  static int _getColorFromHex(String hexColor) {
    hexColor = hexColor.toUpperCase().replaceAll("#", "");
    if (hexColor.length == 6) {
      hexColor = "FF" + hexColor;
    }
    return int.parse(hexColor, radix: 16);
  }

  HexColor(final String hexColor) : super(_getColorFromHex(hexColor));
}

class AppColors {
  static var mainThemeColor = MaterialColor(500, <int, Color>{
    50: Color.fromRGBO(230, 33, 41, 1),
    100: Color.fromRGBO(230, 33, 41, 1),
    200: Color.fromRGBO(230, 33, 41, 1),
    300: Color.fromRGBO(230, 33, 41, 1),
    400: Color.fromRGBO(230, 33, 41, 1),
    500: Color.fromRGBO(230, 33, 41, 1),
    600: Color.fromRGBO(230, 33, 41, 1),
    700: Color.fromRGBO(230, 33, 41, 1),
    800: Color.fromRGBO(230, 33, 41, 1),
    900: Color.fromRGBO(230, 33, 41, 1),
  });

  static var white2 = HexColor("f0f0f0");

  static Color colorForSelectionMode({bool selected}) {
    return selected ? mainThemeColor[500] : Colors.black;
  }
}


class CustomColors {
  static final Color firebaseNavy = Color(0xFF2C384A);
  static final Color firebaseOrange = Color(0xFFF57C00);
  static final Color firebaseAmber = Color(0xFFFFA000);
  static final Color firebaseYellow = Color(0xFFFFCA28);
  static final Color firebaseGrey = Color(0xFFECEFF1);
  static final Color googleBackground = Color(0xFF4285F4);
}