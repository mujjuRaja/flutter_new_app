import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:intl/intl.dart';
import 'package:lottie/lottie.dart';
import 'package:overlay_support/overlay_support.dart';

import 'Constant.dart';

//import 'package:flutter_launch/flutter_launch.dart';

class IntelUtility {
  static void showLoader(bool show, BuildContext context) {
    if (show) {
      showDialog(
          context: context,
          barrierDismissible: false,
          builder: (BuildContext context) {
            return Center(
                child: SpinKitDualRing(
              color: AppTheme.mainThemeColor(),
              size: 60,
            ));
          });
    } else {
      Navigator.of(context, rootNavigator: true).pop("");
    }
  }

  static bool isValidEmail(String email) {
    bool result = RegExp(
            r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+")
        .hasMatch(email);
    return result;
  }

  static Future<dynamic> navigateToScreen(
      BuildContext context, Widget screen) async {
    var value = await Navigator.push(
        context, MaterialPageRoute(builder: (context) => screen));
    return value;
  }

  static navigateReplaceToScreen(BuildContext context, Widget screen) {
    Navigator.pushReplacement(
        context, MaterialPageRoute(builder: (context) => screen));
  }

  static navigateReplaceToScreenClearBack(BuildContext context, Widget screen) {
    Navigator.of(context).pushAndRemoveUntil(
        MaterialPageRoute(builder: (context) => screen),
        (Route<dynamic> route) => false);
  }

  static loading(bool isLoading, BuildContext context) {
    if (isLoading) {
      showDialog(
          context: context,
          barrierDismissible: false,
          builder: (BuildContext context) {
            return Center(
              child: Container(
                width: 100,
                height: 100,
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(10),
                ),
                child: Lottie.asset(IconNames.loading,
                    frameBuilder: (context, child, composition) {
                  return AnimatedOpacity(
                    child: child,
                    opacity: composition == null ? 0 : 1,
                    duration: const Duration(seconds: 1),
                    curve: Curves.easeOut,
                  );
                }),
              ),
            );
          });
    } else {
      Navigator.pop(context);
    }
  }

  static showError(BuildContext context, String error) {
    showSimpleNotification(
      Text(
        error,
        style: TextStyle(fontWeight: FontWeight.w500),
      ),
      background: AppTheme.mainThemeColor(),
      foreground: Colors.black,
      slideDismiss: true,
    );
  }

  static showToast(BuildContext context, String messageValues) {
    final scaffold = ScaffoldMessenger.of(context);
    scaffold.showSnackBar(
      SnackBar(
        content: Text(messageValues),
        action: SnackBarAction(
            label: 'Ok', onPressed: scaffold.hideCurrentSnackBar),
      ),
    );
  }

  static void showAlert(BuildContext context, String title, String message) {
    // set up the button
    AlertDialog alert;

    Widget okButton = FlatButton(
      child: Text("OK"),
      onPressed: () {
        Navigator.of(context, rootNavigator: true).pop(true);
      },
    );

    Widget cancelButton = FlatButton(
      child: Text("Cancel"),
      onPressed: () {
        Navigator.of(context, rootNavigator: true).pop(false);
      },
    );

    // set up the AlertDialog
     alert = AlertDialog(
      title: Text(title),
      content: Text(message),
      actions: [
        okButton,
        cancelButton,
      ],
    );

    // show the dialog
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }

  static convertDateFormat(String date) {
    String returnDate = date;
    DateTime dateTime = DateTime.parse(date);
    if (dateTime != null) {
      DateFormat dateFormat = DateFormat("EEE dd-MM-yyyy");
      return dateFormat.format(dateTime);
    }
    return returnDate;
  }

  static String formatedDate(String strDate) {
    DateTime deptDate = DateTime.parse(strDate);
    final dateFormatter = DateFormat('EEEE dd-MMM-yyyy');
    return dateFormatter.format(deptDate);
  }

  static String convertedDate(String strDate) {
    DateTime deptDate = DateTime.parse(strDate);
    final dateFormatter = DateFormat('dd-MM-yyyy');
    return dateFormatter.format(deptDate);
  }

  static String convertedDateForUpload(String strDate) {
    DateTime deptDate = DateTime.parse(strDate);
    final dateFormatter = DateFormat('yyyy-MM-dd');
    return dateFormatter.format(deptDate);
  }

  static String convertedDateFromServerTimeStamp(String date) {
    var array = date.split(" ");
    var array1 = array[0].split("-");
    var array2 = array[1].split(":");
    final DateTime now = new DateTime(
        int.parse(array1[0]),
        int.parse(array1[1]),
        int.parse(array1[2]),
        int.parse(array2[0]),
        int.parse(array2[1]));
    final DateFormat formatter = DateFormat('dd-MM-yyyy');
    final String formatted = formatter.format(now);
    return formatted;
  }

  static String convertedDateFromServerTimeStampForProfile(String date) {
    var array = date.split(" ");
    var array1 = array[0].split("-");

    final DateTime now = new DateTime(
        int.parse(array1[0]), int.parse(array1[1]), int.parse(array1[2]), 0, 0);
    final DateFormat formatter = DateFormat('dd-MM-yyyy');
    final String formatted = formatter.format(now);
    return formatted;
  }

  static String convertedTimeFromServerTimeStamp(String time) {
    var array = time.split(" ");
    var array1 = array[0].split("-");
    var array2 = array[1].split(":");
    final DateTime now = new DateTime(
        int.parse(array1[0]),
        int.parse(array1[1]),
        int.parse(array1[2]),
        int.parse(array2[0]),
        int.parse(array2[1]));
    final DateFormat formatter = DateFormat('HH:mm');
    final String formatted = formatter.format(now);
    return formatted;
  }

  static String getHoursFromMinutes(int value) {
    final int hour = value ~/ 60;
    final int minutes = value % 60;
    return '${hour.toString().padLeft(2, "0")}:${minutes.toString().padLeft(2, "0")}';
  }

  static void hideKeyBoard(BuildContext context) {
    FocusScope.of(context).requestFocus(FocusNode());
  }
}
