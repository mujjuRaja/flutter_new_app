import 'dart:ui';

import 'package:flutter/material.dart';

class ViewPagerUtils {
  static const lightBlue = Color(0xff00bbff);
  static const mediumBlue = Color(0xff00a2fc);
  static const darkBlue = Color(0xff0075c9);

  static final lightGreen = Colors.green.shade300;
  static final mediumGreen = Colors.green.shade600;
  static final darkGreen = Colors.green.shade900;

  static final lightRed = Colors.red.shade300;
  static final mediumRed = Colors.red.shade600;
  static final darkRed = Colors.red.shade900;
}
