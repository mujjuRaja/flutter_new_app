
import 'package:flutter/cupertino.dart';
import 'package:flutter_app/viewpager/MyBox.dart';

import 'ViewPagerUtils.dart';

class MyPage1Widget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Row(
          children: [
            MyBox(ViewPagerUtils.darkGreen, height: 50),
          ],
        ),
        Row(
          children: [
            MyBox(ViewPagerUtils.lightGreen),
            MyBox(ViewPagerUtils.lightGreen),
          ],
        ),
        MyBox(ViewPagerUtils.mediumGreen, text: 'PageView 1'),
        Row(
          children: [
            MyBox(ViewPagerUtils.lightGreen, height: 200),
            MyBox(ViewPagerUtils.lightGreen, height: 200),
          ],
        ),
      ],
    );
  }
}