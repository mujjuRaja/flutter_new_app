import 'package:flutter/cupertino.dart';
import 'package:flutter_app/viewpager/MyBox.dart';
import 'package:flutter_app/viewpager/ViewPagerUtils.dart';

class MyPage2Widget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Row(
          children: [
            MyBox(ViewPagerUtils.darkBlue, height: 50),
            MyBox(ViewPagerUtils.darkBlue, height: 50),
          ],
        ),
        Row(
          children: [
            MyBox(ViewPagerUtils.lightBlue),
            MyBox(ViewPagerUtils.lightBlue),
          ],
        ),
        MyBox(ViewPagerUtils.mediumBlue, text: 'PageView 2'),
        Row(
          children: [
            MyBox(ViewPagerUtils.lightBlue),
            MyBox(ViewPagerUtils.lightBlue),
          ],
        ),
      ],
    );
  }
}