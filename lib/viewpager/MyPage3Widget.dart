import 'package:flutter/cupertino.dart';
import 'package:flutter_app/viewpager/MyBox.dart';
import 'package:flutter_app/viewpager/ViewPagerUtils.dart';

class MyPage3Widget extends StatefulWidget {
  @override
  _MyPage3WidgetState createState() => _MyPage3WidgetState();
}

class _MyPage3WidgetState extends State<MyPage3Widget> {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Row(
          children: [
            MyBox(ViewPagerUtils.darkRed),
            MyBox(ViewPagerUtils.darkRed),
          ],
        ),
        MyBox(ViewPagerUtils.mediumRed, text: 'PageView 3'),
        Row(
          children: [
            MyBox(ViewPagerUtils.lightRed),
            MyBox(ViewPagerUtils.lightRed),
            MyBox(ViewPagerUtils.lightRed),
          ],
        ),
      ],
    );
  }
}