import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_app/listdata/ItemWidget.dart';

import 'ProductModel.dart';

class ProductListPage extends StatefulWidget {
  @override
  _ProductListPageState createState() => _ProductListPageState();
}

class _ProductListPageState extends State<ProductListPage> {
  //final dummyList = List.generate(10, (index) => ProductModel.items[0]);
  //ProductModel.items listOfItem=List();
  @override
  void initState() {
    super.initState();
    loadData();
  }

  loadData() async {
    await Future.delayed(Duration(seconds: 2));
    final profuctJson = await rootBundle.loadString("assets/product.json");
    final profuctJsonData = jsonDecode(profuctJson);
    var productsData = profuctJsonData["products"];
    ProductModel.items = List.from(productsData)
        .map<Item>((item) => Item.fromMap(item))
        .toList();
    print(productsData);
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("E-Commers App"),
      ),
      //(ProductModel.items != null && ProductModel.items.isNotEmpty) ?
      body: LayoutBuilder(
        builder: (BuildContext context, BoxConstraints constraints) {
          return (ProductModel.items != null && ProductModel.items.isNotEmpty)
              ? ListView.builder(
                  itemCount: ProductModel.items.length,
                  itemBuilder: (context, index) => ItemWidget(
                    items: ProductModel.items[index],
                  ),
                )
              : Center(
                  child: CircularProgressIndicator(),
                );
        },
      ),
    );
  }
}
