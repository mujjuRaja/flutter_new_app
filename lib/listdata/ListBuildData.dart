import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app/utils/Constant.dart';

import 'ProductModel.dart';

class ListBuildData extends StatefulWidget {
  final Item items;

  const ListBuildData({Key key, @required this.items})
      : assert(items != null),
        super(key: key);

  @override
  _ListBuildDataState createState() => _ListBuildDataState();
}

class _ListBuildDataState extends State<ListBuildData> {
  @override
  Widget build(BuildContext context) {
    return
        /* appBar: AppBar(
        backgroundColor: Colors.green,
        title: Text("Login Screen"),
        elevation: 4,
      ),
      body:*/
        Container(
      padding: EdgeInsets.all(10),
      child: Card(
        elevation: 8,
        child: (widget.items != null)
            ? ListTile(
                onTap: () {
                  print("On Tap -==-> ");
                },
                leading: Image.asset(IconNames.applogo),
                title: Text(widget.items.name),
                subtitle: Text(widget.items.description),
                trailing: Text(
                  "\$${500}",
                  style: TextStyle(
                    color: Colors.red,
                    fontSize: 16,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              )
            : Container(
                child: Text("No Data found."),
              ),
      ),
    );
  }
}
