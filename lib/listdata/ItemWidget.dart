
import 'package:flutter/cupertino.dart';
import 'package:flutter_app/listdata/ListBuildData.dart';
import 'package:flutter_app/listdata/ProductModel.dart';


class ItemWidget extends StatelessWidget{
  final Item items;

  const ItemWidget({Key key, @required this.items}) : assert(items != null), super(key: key);
  
  @override
  Widget build(BuildContext context) {
    return Container(
      child: ListBuildData(items:items),
    );
  }
  
}