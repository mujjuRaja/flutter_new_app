class ProductModel {
  static  List<Item> items = [
    //Item(id:"1", name:"iPhone", description:"this", image:"0"),
  ];
}

class Item {
  final String id;
  final String name;
  final String description;
  final String image;

  Item({this.id, this.name, this.description, this.image});

  //Encode Json
  factory Item.fromMap(Map<String, dynamic> json){
    return Item(
      id: json["id"],
      name: json["name"],
      description: json["description"],
      image: json["image"],
    );
  }

  //Decode Json
  toMap() =>{
    "id":id,
    "name":name,
    "description":description,
    "image":image,
  };
}
